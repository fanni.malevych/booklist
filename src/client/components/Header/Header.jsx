import React from 'react';

const Header = () => {
    return (
        <>
            <h1 className="display-4 text-center"><i className="fas fa-book-open text-primary"></i> <span
                className="text-secondary">Book</span> List</h1>
            <p className="text-center">Add your book information to store it in database.</p>
        </>
    )
};

export default Header