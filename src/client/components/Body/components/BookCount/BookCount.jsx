import React from 'react';

const BookCount = ({count}) => {
    return (
        <h3 id="book-count" className="book-count mt-5">Всего книг: {count}</h3>
    )
};

export default BookCount