import React, {Component} from 'react';
import Input from "../../../../../shared/components/Input";
import Button from "../../../../../shared/components/Button";
import {fields} from "../../../../../shared/components/Input/fields";

class Book extends Component {
    state = {
        edit: false,
        title: '',
        author: '',
        isbn: ''
    };

    onEdit = () => {
        const {title, author, isbn} = this.props;
        this.setState({
            edit: true,
            title, author, isbn
        });
    };

    onSave = (e) => {
        e.preventDefault();
        const {title, author, isbn} = this.state;
        this.props.updateBook(title, author, isbn);
        this.setState({edit: false});
    };

    handleChange = ({target}) => {
        this.setState({
            [target.name]: target.value
        });
    };

    render() {
        const {edit} = this.state
        const {handleChange, onSave, onEdit} = this
        if (edit) {
            const {title: titleInput, author: authorInput, isbn: isbnInput} = fields
            const {title, author, isbn} = this.state;
            return (
                <tr>
                    <td>
                        <div className="form-group">
                            <Input {...titleInput} value={title} handleChange={handleChange}/>
                        </div>
                    </td>
                    <td>
                        <div className="form-group">
                            <Input {...authorInput} value={author} handleChange={handleChange}/>
                        </div>
                    </td>
                    <td>
                        <div className="form-group">
                            <Input {...isbnInput} value={isbn} required handleChange={handleChange}/>
                        </div>
                    </td>
                    <td><Button type="primary" text="Update" onClick={onSave}/>
                    </td>
                    <td></td>
                </tr>
            )
        } else {
            const {title, author, isbn, deleteBook} = this.props
            return (
                <tr>
                    <td>{title}</td>
                    <td>{author}</td>
                    <td>{isbn}</td>
                    <td><a href="#" className="btn btn-info btn-sm" onClick={onEdit}><i
                        className="fas fa-edit"></i></a></td>
                    <td><a href="#" className="btn btn-danger btn-sm btn-delete" onClick={deleteBook}>X</a></td>
                </tr>
            )
        }
    }
}

export default Book