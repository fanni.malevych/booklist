import React, {Component} from 'react';
import AddBookForm from "../AddBookForm";
import BookCount from "../BookCount";
import BookList from "../BookList";

class BookApp extends Component {

    state = {
        bookList: []
    };

    addBook = (title, author, isbn,) => {
        const newBook = {title, author, isbn};
        this.setState(({bookList}) => {
            return {
                bookList: [...bookList, newBook]
            }
        })
    };

    deleteBook = (idx) => {
        this.setState(({bookList}) => {
            const newBookList = [...bookList];
            newBookList.splice(idx, 1);
            return {
                bookList: newBookList
            }
        })
    };
    updateBook = (index, title, author, isbn) => {
        this.setState(({bookList}) => {
            const newBookList = [...bookList];
            newBookList[index] = {title, author, isbn};
            return {
                bookList: newBookList
            }
        })
    };

    render() {
        const {bookList} = this.state;
        const count = bookList.length;
        const {addBook, deleteBook, updateBook} = this
        return (
            <>
                <div className="row">
                    <div className="col-lg-4">
                        <AddBookForm addBook={addBook}/>
                    </div>
                </div>
                <BookCount count={count}/>
                <BookList bookList={bookList} deleteBook={deleteBook} updateBook={updateBook}/>
            </>
        )
    }
}

export default BookApp