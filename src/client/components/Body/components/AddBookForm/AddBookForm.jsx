import React, {Component} from 'react';
import Input from "../../../../../shared/components/Input";
import Button from "../../../../../shared/components/Button";
import {fields} from "../../../../../shared/components/Input/fields";
import {ErrorMsg} from "../../../../../shared/components/Input/errorMsg";

class AddBookForm extends Component {
    state = {
        title: '',
        author: '',
        isbn: '',
        errorName: '',
        errorAuthor:'',
        errorIsbn:''
    };

    handleChange = ({target}) => {
        this.setState({
            [target.name]: target.value
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {title, author, isbn} = this.state;
        if (title && author && isbn) {
            this.props.addBook(title, author, isbn);
            this.setState({
                title: '',
                author: '',
                isbn: '',
                errorName: '',
                errorAuthor:'',
                errorIsbn:''
            })
        }
        if (!title) {
            this.setState({
                errorName: <ErrorMsg />
            })
        }
        if(!author){
            this.setState({
                errorAuthor: <ErrorMsg />
            })
        }
        if(!isbn){
            this.setState({
                errorIsbn: <ErrorMsg />
            })
        }
    };

    render() {
        const {title: titleInput, author: authorInput, isbn: isbnInput, submit} = fields
        const {title, author, isbn, errorAuthor, errorIsbn, errorName} = this.state;
        const {handleSubmit, handleChange} = this
        return (
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <Input {...titleInput} value={title} handleChange={handleChange}/>
                    {errorName}
                </div>

                <div className="form-group">
                    <label htmlFor="author">Author</label>
                    <Input {...authorInput} value={author} handleChange={handleChange}/>
                    {errorAuthor}
                </div>
                <div className="form-group">
                    <label htmlFor="title">ISBN#</label>
                    <Input {...isbnInput} value={isbn} handleChange={handleChange}/>
                    {errorIsbn}
                </div>
                <Button {...submit}/>
            </form>
        )
    }
}

export default AddBookForm