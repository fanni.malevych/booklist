import React from 'react';
import Book from "../Book";

const BookList = ({bookList, deleteBook, updateBook}) => {
    const bookItems = bookList.map((book, index) => <Book {...book}
                                                          deleteBook={() => deleteBook(index)}
                                                          updateBook={(title, author, isbn) => updateBook(index, title, author, isbn)}/>);
    return (
        <table className="table table-striped mt-2">
            <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>ISBN#</th>
            </tr>
            </thead>
            <tbody>
            {bookItems}
            </tbody>
        </table>
    )
};

export default BookList