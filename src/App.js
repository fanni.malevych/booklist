import React from 'react';
import './App.css';
import Header from "./client/components/Header";
import Body from "./client/components/Body/components/Body";

function App() {
    return (
        <div className="container mt-4">
            <Header/>
            <Body/>
        </div>
    );
}

export default App;
