import React from "react";

const Button = (props) => {
    const {className, text, onClick} = props;
    const fullClassName = `btn ${className || ''}`
    return (
        <button className={fullClassName} onClick={onClick}>{text}</button>
    )
}

export default Button

