import React from "react";

export const ErrorMsg = () => {
    return (
        <div className="text-danger font-italic">
            это поле обязательно к заполнению!
        </div>
    )
}