export const fields = {
    title: {
        name: 'title',
    },
    author: {
        name: 'author'
    },
    isbn: {
        name: 'isbn'
    },
    submit: {
        text: 'Add book',
        className: 'btn-primary'
    }
}